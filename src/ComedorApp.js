import { LitElement, html, css } from 'lit';
import './ComedorHeader';
import './ComedorMain';

const logo = new URL('../assets/open-wc-logo.svg', import.meta.url).href;

export class ComedorApp extends LitElement {
  static get properties() {
    return {
      title: { type: String },
    };
  }

  static get styles() {
    return css`
      main {
        flex-grow: 1;
      }

      .logo {
        margin-top: 36px;
        animation: app-logo-spin infinite 20s linear;
      }

      @keyframes app-logo-spin {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
      }

      .app-footer {
        font-size: calc(12px + 0.5vmin);
        align-items: center;
      }

      .app-footer a {
        margin-left: 5px;
      }
    `;
  }

  constructor() {
    super();
    this.title = 'My app';
  }

  render() {
    return html`
      <main>
        <comedor-header></comedor-header>
        <comedor-main></comedor-main>
    </main>
    `;
  }
}
