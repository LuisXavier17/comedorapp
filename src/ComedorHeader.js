import {LitElement, html, css} from 'lit';
import sandbox from './Sandbox';

export class ComedorHeader extends LitElement {
  static get properties() {
    return {
      firstName: { type: String },
      lastName: { type: String },
      login: { type: Boolean }
    }
  }

  constructor() {
    super();
    this.firstName = '';
    this.lastName = '';
    this.login = false;
  }

  connectedCallback() {
    super.connectedCallback();
    sandbox.on('login-validate', this.loginValidation.bind(this));
  }

  loginValidation(e){
    this.login = e.login;
    this.firstName = e.userData.name;
    this.lastName = e.userData.lastName;
  }

  seleccionarComedor(comedor) {
    sandbox.dispatch('seleccionar-comedor', comedor, this);
  }

  liberarLugar() {
    sandbox.dispatch('consultar-lugar', null, this);
  }

  render() {
    return html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">Gestión de comedor</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a href="#" class="nav-link" style="color: #FF0080" @click="${(e) => this.seleccionarComedor("r")}">Comedor Rosa</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link" style="color: #008F39" @click="${(e) => this.seleccionarComedor("v")}">Comedor Verde</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link" style="color: #FF8000" @click="${(e) => this.seleccionarComedor("n")}">Comedor Naranja</a>
          </li>
          <button class="btn btn-success" @click="${this.liberarLugar}">Liberar lugar</button>
        </ul>
        <span class="navbar-text">
          ${this.login === true ? html`Bienvenido: ${this.firstName + ' ' + this.lastName}`:html``}
        </span>
      </div>
    </nav>
    `;
  }
}

customElements.define('comedor-header', ComedorHeader);