import { LitElement, html, css } from 'lit-element';
import sandbox from '../Sandbox';

class LoginComedor extends LitElement{

    static get styles(){
        return css`

        .main{
            display: flex;
            justify-content: center;
            margin-top: 10%;
        }

        .form {
            display: flex;
            flex-direction: column;
            width: 25%;
            padding: 20px;
            color: white;
            background-color: #14549C;
        }

        .form-group{
            display: flex;
            flex-direction: column;
        }


        p{
            color: red;
            margin-bottom: 0 !important;  
        }

        .error{
            margin-top: 18px;
            background-color: white;
        }

        `;
    }

    static get properties(){
        return{
            user: { type: String },
            pass: { type: String },
            messageError: { type: String }
        }
    }

    constructor(){
        super();
        this.user = "";
        this.pass = "";
        this.messageError = "";
    }

    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <div class="main">
          <div class="form border rounded border-primary">
            <h4>Reserva de lugar en comedor</h4>
            <div class="form-group">
                <label>Ingresa tu correo corporativo</label>
                <input type="text"/ @input="${this.updateUser}">
            </div>
            <div class="form-group">
                <label>Ingresa tu contraseña</label>
                <input type="password" @input="${this.updatePass}"/>
            </div>
            <button class="btn-default" @click="${this.validateUser}"><strong>Ingresar</strong></button>
                ${this.messageError ? html`<div class="error"><p class="alert" text-align="center">${this.messageError}</p></div>`: ''} 
          </div>
        </div>
        `;
    }

    updateUser(e){
        this.user = e.target.value;
    }

    updatePass(e){
        this.pass = e.target.value;
    }

    validateUser(){
        let validation = true;
        let inputForm = this.shadowRoot.querySelectorAll('input');
        inputForm.forEach(inp => {
            if(inp.value === "" || inp.value === undefined){
                validation = false;
                this.formBorderColor(inp, false);
                this.messageError = "Campos incompletos";
            }else{
                this.formBorderColor(inp, true);
                this.messageError = "";
            }
        })
        if(validation){
        let validateUser = {
            email: this.user,
            password: this.pass
        }
        this.ajaxRequest(validateUser);
        }
    }

    validateInputColor(){

    }


    ajaxRequest(post){
        const jsonPost= JSON.stringify(post);
        let xhr = new XMLHttpRequest();
        xhr.open("POST", "http://localhost:3001/login", true);
        xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
        xhr.onload = function(){
            if(xhr.status === 200){
                let res = JSON.parse(xhr.responseText);
                this.goPage(res);
            }else{
                this.messageError = 'Error al autenticar, vuelva a intentar'
            }
        }.bind(this);
        xhr.send(jsonPost);
    }

    formBorderColor(inp, validate){
        if(validate){
            inp.style.border = "1px solid #ced4da";
        }else{
            inp.style.border = "3px solid red";
        }
    }

    goPage(res){
        sandbox.dispatch('login-validate', {"userData":res.user, "login": true}, this);
        /*
        this.dispatchEvent(new CustomEvent('login-validate',{
            detail: {
                data: {"userData": res.user, "login": true}
            },
            bubbles: true,
            composed: true
        }))
        */
    }


}

customElements.define('login-comedor', LoginComedor)