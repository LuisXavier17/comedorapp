import { LitElement, html, css } from 'lit';
import sandbox from './Sandbox';
import $ from './ajax';
import './comedor-listado-mesas';
import './comedor-modal';

export class ComedorMain extends LitElement {
  static get properties(){
    return {
      comedoresArray: { type: Array },
      mesasArray: { type: Array },
      sillasArray: { type: Array },
      login: { type: Boolean},
      userEmail: {type: String },
      modalTitle: { type: String },
      modalAccept: { type: String },
      modalCancel: { type: String },
      modalBody: { type: String },
      identifier: { type: String }
    }
  }

  constructor() {
    super();
    this.comedoresArray = [];
    this.mesasArray = [];
    this.sillasArray = [];
    this.login = false;
    this.userEmail = '';
    this.modalTitle = '';
    this.modalAccept = '';
    this.modalCancel = '';
    this.modalBody = '';
    this.identifier = '';

  }

  connectedCallback() {
    super.connectedCallback();
    sandbox.on('seleccionar-comedor', this.seleccionarComedor.bind(this));
    sandbox.on('login-validate', this.loginValidation.bind(this));
    sandbox.on('close-modal', this.closeModal.bind(this));
    sandbox.on('confirm-modal', this.confirmModal.bind(this));
    sandbox.on('consultar-lugar', this.consultarLugar.bind(this));
    this.seleccionarComedor("r");
  }

  consultarLugar() {
    $.ajax(
      'GET',
      'http://localhost:3002/comedorusuario/' + this.userEmail,
      function (response) {
        this.modalTitle = 'Liberar silla';
        this.modalBody = '¿Desea liberar su silla?';
        this.modalAccept = 'Confirmar';
        this.modalCancel = 'Cancel';
        this.identifier = response.mesa+'-'+response.silla+'-edit';
        this.shadowRoot.getElementById('modal').classList.toggle('d-none');
      }.bind(this)
    );
  }

  

  seleccionarSilla(e) {
    $.ajax(
      'GET',
      'http://localhost:3002/comedorusuario/' + this.userEmail,
      function (response) {
        if (Object.entries(response).length === 0) {
          this.modalTitle = 'Elegir silla';
          this.modalBody = '¿Desea elegir esta silla?';
          this.modalAccept = 'Confirmar';
          this.modalCancel = 'Cancel';
          this.identifier = e.detail.ubicacionSilla + '-add';
        } else {
          this.modalTitle = 'Alerta';
          this.modalBody = 'Ya cuentas con una silla asignada, por favor libera tu lugar e intenta nuevamente';
          this.modalAccept = 'Confirmar';
          this.modalCancel = 'Cancel';
          this.identifier = e.detail.ubicacionSilla + '-cancel';
        }
        this.shadowRoot.getElementById('modal').classList.toggle('d-none');
      }.bind(this)
    );
    

  }
  
  closeModal() {
    this.shadowRoot.getElementById('modal').classList.toggle('d-none');
  }

  confirmModal(e) {
    let mesaId = e.substring(0, 2);
    let sillaId = e.match(/-(.*?)-/i)[1];
    console.log(mesaId);
    console.log(sillaId);
    if (e.substring(e.lastIndexOf('-') + 1) === 'add') {
        this.comedoresArray.mesas.find(mesa => mesa.id === mesaId).sillas[sillaId] = false;
        this.postComedores(this.comedoresArray.id, this.comedoresArray);
        this.postSillaUsuario(mesaId, sillaId);
    } else if (e.substring(e.lastIndexOf('-') + 1) === 'edit'){
      console.log("Liberar");
      this.deleteLiberarLugar(mesaId, sillaId);
    }
    this.closeModal();
  }

  deleteLiberarLugar(mesaId, sillaId) {
    this.comedoresArray.mesas.find(mesa => mesa.id === mesaId).sillas[sillaId] = true;
    
    this.postComedores(this.comedoresArray.id, this.comedoresArray);

    $.ajax(
      'DELETE',
      'http://localhost:3002/comedorusuario/'+this.userEmail,
      function (response) {
        console.log(response)
      }.bind(this),
    );
  }

  postSillaUsuario(mesa, silla) {
    let registro = new Object();
    registro['id'] = this.userEmail;
    registro['comedor'] = this.comedoresArray.id;
    registro['mesa'] = mesa;
    registro['silla'] = silla;
    $.ajax(
      'POST',
      'http://localhost:3002/comedorusuario',
      function(response){
        console.log(response)
      }.bind(this),
      registro
    );
  }


  loginValidation(e) {
    this.userEmail = e.userData.email;
    this.login = e.login;
  }
  
  
  seleccionarComedor(comedor){
    $.ajax(
      'GET',
      'http://localhost:3000/comedores/'+comedor,
      function(response){
        this.comedoresArray = response;
        this.mesasArray = this.comedoresArray.mesas;
      }.bind(this)
    );
  }

 

  actualizarDisponibilidad(e) {
    /*
    const mesaElegida = e.detail.ubicacionSilla.split('-')[0];
    const sillaelegida = e.detail.ubicacionSilla.split('-')[1];
  
    this.comedoresArray.mesas.forEach(
      (mesa, index) => {
        if(mesa.id === mesaElegida) {
          if(sillaelegida === 's1') {
            mesa.sillas.sillaUno = !Boolean(mesa.sillas.sillaUno);
          }
          if(sillaelegida === 's2') {
            mesa.sillas.sillaDos = !Boolean(mesa.sillas.sillaDos);
          }
        } 
      }
    );
    console.log('actualizado: '+JSON.stringify(this.comedoresArray));
    */
  }

  postComedores(id, comedor) {console.log(id);
    console.log(comedor);
    $.ajax(
      'PUT',
      'http://localhost:3000/comedores/'+id,
      function(response) {
        this.seleccionarComedor(id)
      }.bind(this),
      comedor
    );
  }

  render() {
    return html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <comedor-modal
      id="modal"
      .identifier="${this.identifier}"
      class="d-none"
      headerTitle="${this.modalTitle}"
      acceptButton="${this.modalAccept}"
      .body="${this.modalBody}"
      declineButton="${this.modalCancel}">
    </comedor-modal>
    ${this.login === true ? html`
      <div class="container">
        <div class="col-md-6 offset-md-3">
          <h1>Comedor ${this.comedoresArray.color}</h1>
          <comedor-listado-mesas
            comedorId="${this.comedoresArray.id}"
            .mesas="${this.mesasArray}"
            @ubicacion-silla="${this.seleccionarSilla}"
          >
          </comedor-listado-mesas>
        </div>
      </div>
    ` : html`<login-comedor></login-comedor>`}
    `;
  }

  static get styles() {
    return css`
    h1{
      text-align: center;
    }
    `
  }
}

customElements.define('comedor-main', ComedorMain);