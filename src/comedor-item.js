import { LitElement, html, css } from "lit-element";
import sandbox from './Sandbox';

class ComedorItem extends LitElement {
    static get properties() {
        return {
            mesas: {type: Array},
            sillas: {type: Array},
            nombreMesa: { type: String },
            comedorId: {type: String},
        }
    }

    static get styles() {
        return css`
            .mesa {
                width: 80px;
                height: 160px;
                border: solid 1px;
            }
            
            .mesa-rosa {
                background-color: #FF0080;
            }

            .mesa-verde {
                background-color: #008F39;
            }

            .mesa-naranja {
                background-color: #FF8000;
            }

            .silla-1 {
                background-color: #FFFFFF;
                width: 40px;
                height: 40px;
                border: solid 1px;
            }

            .silla-2 {
                background-color: #FF8000;
                width: 40px;
                height: 40px;
                border: solid 1px;
            }

            .disponible {
                background-color: #FFFF00;
            }

            .noDisponible {
                background-color: #FF0000;
            }
        `;
    }

    constructor() {
        super();

        this.mesas = []
        this.sillas = [];
        this.nombreMesa = '';
    }

    connectedCallback() {
        super.connectedCallback();
        
        //this.colorComedor(this.comedorId);
    }

    updated() {
        super.updated();
        this.colorComedor(this.comedorId);
        //this.disponibilidadSillas(this.mesas.sillas.sillaUno, this.mesas.sillas.sillaDos);
    }

    elegirSilla(e) {
        const sillaElegida = e.path[0]. id;

        sandbox.dispatch(
            'ubicacion-silla',
            {
                'ubicacionSilla': sillaElegida
            }, this
        );
    }

    /*
    disponibilidadSillas(s1, s2) {
        const silla1 = 'silla-1 ' + this.colorSillaDisponible(s1);
        const silla2 = 'silla-2 ' + this.colorSillaDisponible(s2);

        this.shadowRoot.getElementById('s1').setAttribute('class', silla1);
        this.shadowRoot.getElementById('s2').setAttribute('class', silla2);
    }
    */

    colorSillaDisponible(disponible) {
        if(disponible) {
            return 'disponible';
        } else {
            return 'noDisponible';
        }
    }

    colorComedor(idComedor) {
        switch(idComedor) {
            case 'r':   this.shadowRoot.getElementById('mesa').setAttribute('class','mesa mesa-rosa');
                        break;
            case 'v':   this.shadowRoot.getElementById('mesa').setAttribute('class','mesa mesa-verde');
                        break;
            case 'n':   this.shadowRoot.getElementById('mesa').setAttribute('class','mesa mesa-naranja');
                        break;
        }
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div class="comedor-container container w-100">
                <div class="comedor row">
                    <div class="col align-self-start d-flex justify-content-end"">
                        <div
                            style="${this.sillas.sillaUno === true ? "background:green;": "background:red;"}"
                            id="${this.mesas.id +'-sillaUno'}"
                            class="silla-1"
                            @click="${this.elegirSilla}">
                        </div>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <div
                            id="mesa" 
                            class="mesa">
                            ${this.mesas.id}
                        </div>
                    </div>
                    <div class="col align-self-end d-flex justify-content-start">
                        <div
                            style="${this.sillas.sillaDos === true ? "background:green;": "background:red;"}"
                            id="${this.mesas.id + '-sillaDos'}"
                            class="silla-2"
                            @click="${this.elegirSilla}">                        
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
}

customElements.define('comedor-item', ComedorItem);