import { LitElement, html, css } from "lit-element";
import './comedor-item.js';
import './login/login-comedor.js';

class ComedorListadoMesas extends LitElement {
    static get properties() {
        return {
            title: {type: String},
            comedorId: {type: String},
            mesas: {type: Array}
        };
    }

    static get styles() {
        return css`
            .mesas {
                width: 500px;
            }
        `;
    }

    constructor() {
        super();

        this.title = '';
        this.comedorId = '';
        this.mesas = [];
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div class="listado-comedores col m-2">
                <h1>${this.title}</h1>
                <ul class="mesas m-2">
                    ${this.mesas.map(
                        function(mesa) {
                            return html`
                                <comedor-item
                                    class="col m-2"
                                    comedorId="${this.comedorId}"
                                    .mesas="${mesa}"
                                    .sillas=${mesa.sillas}
                                >
                                </comedor-item>
                            `;
                        }.bind(this)
                    )}
                </ul>
            </div>
        `;
    }
}

customElements.define('comedor-listado-mesas', ComedorListadoMesas);