import {LitElement, html, css, supportsAdoptingStyleSheets} from 'lit'
import sandbox from './Sandbox';

export class ComedorModal extends LitElement {
  static get properties() {
    return {
      headerTitle: { type: String },
      body: { type: String },
      acceptButton: { type: String },
      declineButton: { type: String },
      identifier: {type: String }
    }
  }

  constructor() {
    super();
    this.headerTitle = '';
    this.body = '';
    this.acceptButton = '';
    this.declineButton = '';
    this.identifier = '';
  }

  closeModal() {
    sandbox.dispatch('close-modal', null, this);
  }

  confirmModal(e) {
    sandbox.dispatch('confirm-modal', e, this);
  }

  render() {
    return html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <div class="modal-container">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">${this.headerTitle}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="${this.closeModal}">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ${this.body}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal" @click="${(e) => this.confirmModal(this.identifier)}">${this.acceptButton}</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="${this.closeModal}">${this.declineButton}</button>
          </div>
        </div>
      </div>
    </div>`;
  }

  static get styles() {
    return css`
      .modal-container {
        position: fixed;
        z-index: 1000;
        top: 0px;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .4);
      }

      .modal-dialog {
        top: 0;
        transform: translateY(120%);
      }
    `;
  }
}

customElements.define('comedor-modal', ComedorModal);