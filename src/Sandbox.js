const sandbox = {
  on(event, callback) {
    document.addEventListener(event, (e) => callback(e.detail));
  },

  dispatch(event, data, context) {
    context = context || document;
    context.dispatchEvent(new CustomEvent(event, { bubbles: true, composed: true, detail: data }));
  }
}

export default sandbox;