const $ = {
    ajax(httpMethod, url, callbackResponse, request) {
        let xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if(xhr.status == 200 || xhr.status == 201) {
                let response = JSON.parse(xhr.response);
                callbackResponse(response);
            }
            else {
                let response = JSON.parse(xhr.response);
                callbackResponse(response);
            }
        }
        xhr.open(httpMethod, url);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Cache-Control', 'no.cache');
        xhr.send(JSON.stringify(request));
    }
}

export default $;