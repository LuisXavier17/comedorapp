import { LitElement, html,css } from 'lit-element';
//import './queso-item.js';
//import '../ajax/ajax';
import sandbox from '../sandbox/sandbox';
//import '../dialog/dialog-app.js';
//import './queso-ficha.js';

class ComedorList extends LitElement {

	static get properties() {
        
		return {
                    src:{type:String},
                    comedores:{type:Map}
                };
    }

	
	static get styles()
	{
		return css `main{margin-top:60px; margin-bottom:70px;}`;
	}

	
	constructor() {
		super();
		this.comedores = [];
		this.showPersonForm = false;
	}

	//src property 
	connectedCallback()
	{ 
		super.connectedCallback();
		this.getData(this.src);
	}

	render() {

    		return html `
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
                <main>
                        <div class="row" id="comedorCatalog">
                                <div class="row row-cols-2 row-cols-md-4 mb-3">

                                ${this.comedores.map(function(comedor){
                                   
                                        return html `
                                                <comedor-item
													id="${comedor.id}"
                                                    name="${comedor.name}"
                                                    .image="${comedor.image}"
                                                    recommended="${comedor.recommended}"
													description="${comedor.description}"
                                                    @reservar-comedor=${this.reservarComedorConfirm}
                                                    @info-comedor=${this.infoComedor}
                                                ></comedor-item>`;
                                }.bind(this))}

                                </div>
                        </div>

                        <div class="row justify-content-center">
							<dialog-app class='d-none'
									@dialog-confirm="${this.reservarComedor.bind(this)}"
									id="confirm" ></dialog-app>
                        </div>
                </main>		

		`;
  	}


  	infoComedor(e) {
		console.log("informacion de lugares disponibles");
  	}

	updated() {
		sandbox.dispatch('comedor-count', {'count':this.comedores.length}, this);
	}

    getData(url) 
      {
        let xhr = new XMLHttpRequest();

        xhr.onload = function(){

            if (xhr.status === 200) {
                console.log("Peticion completada satisfactoriamnet");

                this.cheeses = JSON.parse(xhr.responseText);
                console.log(this.comedores);

            }

        }.bind(this);

        xhr.open("GET", url);
        xhr.send();
    }	

}

customElements.define('comedor-list', ComedorList);
